import axios from "axios"
import { BASEURL } from "../const"

export const getBooks = async () => {

    try{
        let response = await axios.get(`${BASEURL}/books`);
        return response.data;
    }catch(error){
        throw new Error(error.message);
    }
}

export const addBook = async (book) => {

    try{
        let response = await axios.post(`${BASEURL}/books`,book);
        console.log(response);
        return "Book Added successfully";
    }catch(error){
        throw new Error(error.message);
    }
}

export const updateBook = async (book) => {

    try{
        let response = await axios.put(`${BASEURL}/books/${book.id}`,book);
        console.log(response);
        return "Book updated successfully";
    }catch(error){
        throw new Error(error.message);
    }
}

export const deleteBook = async (id) => {

    try{
        let response = await axios.delete(`${BASEURL}/books/${id}`);
        console.log(response);
        return "Book deleted successfully";
    }catch(error){
        throw new Error(error.message);
    }
}

export const getGenres = async () => {

    try{
        let response = await axios.get(`${BASEURL}/genres`);
        return response.data;
    }catch(error){
        throw new Error(error.message);
    }
}
