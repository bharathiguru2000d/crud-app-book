import { Box } from "@mui/material";
import { useEffect, useState } from "react";
import {  getBooks } from "../service/bookService";
import BookTable from "./BookTable";
import Header from "./Header";

const ListOfBooks = () => {
    
    const [books,setBooks] = useState([]);

    const [filteredBook , setFilteredBook] = useState("");

    const [formModal , setFormModal] = useState(false);

    const [update,setUpdate] = useState(false);

    const [bookData , setBookData] = useState({
        id : "",
        title : "",
        author : "",
        description :"",
        genre :""
    });

    useEffect(() => {
        console.log(bookData);
    }, [bookData])
    

    useEffect(() => {
        getBooks().then( data => setBooks(data)).catch(data => console.log(data.message));
        return()=>{
            setBooks([]);
        }
    }, [])


    return ( 
        <Box sx={{ px : {xs : 2,sm: 3 , md : 5} }}  py={3}>
            <Header setUpdate={setUpdate} update={update} setFilteredBook={setFilteredBook} setBooks={setBooks} formModal={formModal} bookData={bookData} setBookData={setBookData} setFormModal={setFormModal} />
            <BookTable setBooks={setBooks} setUpdate={setUpdate} setFormModal={setFormModal} filteredBook={filteredBook} setBookData={setBookData} books={books}  />
        </Box>
     );
}
 
export default ListOfBooks;