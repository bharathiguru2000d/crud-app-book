import { Button, Grid, InputAdornment, TextField, Typography } from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import SearchIcon from '@mui/icons-material/Search';
import Form from "./Form";

const Header = ({ update, setUpdate,setFilteredBook ,formModal,setFormModal, bookData, setBookData ,setBooks}) => {
    return ( 
        <Grid container display={"flex"} justifyContent={"space-between"} sx={{my : {xs : 1 , md :2 }}} alignItems={"center"}  >
            <Grid item >
                <Typography variant="h4" sx={{ fontSize : {xs: 15 , sm:30, md :40} }} fontWeight={"bold"} >List Of Books</Typography>
            </Grid>
            <Grid item display={"flex"} >
                    <TextField onChange={(e)=>setFilteredBook(e.target.value)} sx={{display : {xs: "none" , sm : "inline-block"}}} InputProps={{startAdornment : <InputAdornment position="start" > <SearchIcon /> </InputAdornment>}} placeholder="search here..." />
                    <Button  sx={{fontWeight : "bold" ,ml :3 ,  }} onClick={()=>setFormModal(true)} variant="contained" startIcon={<AddIcon />} >Add Book</Button>
            </Grid>
            <Form setUpdate={setUpdate} update={update} bookData={bookData} setBookData={setBookData} formModal={formModal} setFormModal={setFormModal} setBooks={setBooks}/>
        </Grid>
     );
}
 
export default Header;