import * as React from "react";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import { useEffect } from "react";
import { Box, Divider, MenuItem, Select, TextField } from "@mui/material";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { addBook, getBooks, getGenres, updateBook } from "../service/bookService";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

const schema = yup
  .object({
    title: yup.string().required(),
    author: yup.string().required(),
    description: yup.string().required(),
    genre: yup.string().required("experience is required"),
  })
  .required();

export default function Form({
  update,
  setUpdate,
  formModal,
  setFormModal,
  bookData,
  setBookData,
  setBooks,
}) {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    watch,
    setValue
  } = useForm({
    defaultValues: {
      title: bookData.title,
      author: bookData.author,
      description: bookData.description,
      genre: bookData.genre,
    },
    resolver: yupResolver(schema),
  });

  const [open, setOpen] = React.useState(false);
  const [manualValue, setManualValue] = React.useState("");
  const [genres, setGenres] = React.useState([]);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    setUpdate(false);
    setBookData({
      id: "",
      title: "",
      author: "",
      description: "",
      genre: "",
    });
    setFormModal(false);
  };

  useEffect(() => {
    getGenres()
      .then((data) => setGenres(data))
      .catch((data) => console.log(data.message));
  }, []);

  useEffect(() => {
    if (formModal) {
      handleClickOpen();
    }
  }, [formModal]);

  const onSubmit = (data) => {

    if (!update) {
      let book = { ...data, id: Math.floor(Math.random() * 100000) };
      addBook(book)
        .then((data) => alert(data))
        .then(getBooks)
        .then((data) => setBooks(data));
    } else {
      let book = { ...data, id: bookData.id };
      updateBook(book)
        .then((data) => alert(data))
        .then(getBooks)
        .then((data) => setBooks(data));
    }
    setUpdate(false);
    handleClose();
  };

  useEffect(() => {
    
    reset({
      title: bookData.title,
      author: bookData.author,
      description: bookData.description,
      genre: bookData.genre
    });


  }, [bookData,reset]);

  return (
    <>
      {genres.length !== 0 && (
        <React.Fragment>
          <BootstrapDialog
            onClose={handleClose}
            aria-labelledby="customized-dialog-title"
            open={open}
          >
            <Box>
              <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
                Add Book
              </DialogTitle>
              <IconButton
                aria-label="close"
                onClick={handleClose}
                sx={{
                  position: "absolute",
                  right: 8,
                  top: 8,
                  color: (theme) => theme.palette.grey[500],
                }}
              >
                <CloseIcon />
              </IconButton>
            </Box>
            <Divider />
            <Box
              sx={{ p: 3 }}
              dividers
              component={"form"}
              onSubmit={handleSubmit(onSubmit)}
            >
              <TextField
                error={Boolean(errors.title)}
                sx={{ mb: 2 }}
                {...register("title")}
                fullWidth
                placeholder="title"
              />
              <TextField
                error={Boolean(errors.author)}
                {...register("author")}
                fullWidth
                sx={{ my: 2 }}
                placeholder="author"
              />
              <TextField
                error={Boolean(errors.description)}
                {...register("description")}
                fullWidth
                sx={{ my: 2 }}
                multiline
                placeholder="description"
              />
              <Select
                fullWidth
                placeholder="select Genre"
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                error={Boolean(errors.genre)}
                {...register("genre")}
                value={manualValue !== "" ? manualValue : watch("type")}
                onChange={(event) => {
                  const selectedValue = event.target.value;
                  setValue("genre", selectedValue);
                  setManualValue(selectedValue);
                }}
              >
                {genres.map((genre) => (
                  <MenuItem key={genre.id} value={genre.name}>
                    {genre.name}
                  </MenuItem>
                ))}
              </Select>
              <Box display={"flex"} justifyContent={"flex-end"} sx={{ my: 2 }}>
                <Button type="submit" variant="contained">
                  {update ? "Update" : "Save"}
                </Button>
                <Button
                  variant="outlined"
                  onClick={() => handleClose()}
                  color="error"
                  sx={{ ml: 2 }}
                >
                  cancel
                </Button>
              </Box>
            </Box>
          </BootstrapDialog>
        </React.Fragment>
      )}
    </>
  );
}
