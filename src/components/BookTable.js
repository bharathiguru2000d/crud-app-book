import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { IconButton } from '@mui/material';
import { deleteBook, getBooks } from '../service/bookService';




export default function BookTable({books, setBooks, filteredBook , setBookData, setUpdate, setFormModal }) {

  const handleDelete = (id)=>{

              if( window.confirm("are you sure you want to delete")){
                deleteBook(id)
                .then(data =>alert(data))
                .then(getBooks)
                .then(data => setBooks(data))
                .catch(data =>console.log(data.message)) 
              }
               
  }
  return (
    <TableContainer component={Paper} >
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead  sx={{ bgcolor : 'primary.main'  }} >
          <TableRow  >
          <TableCell sx={{color : "white" , fontWeight : "bold" }} >Id</TableCell>
            <TableCell sx={{color : "white" , fontWeight : "bold" }} >Title</TableCell>
            <TableCell sx={{color : "white" , fontWeight : "bold" }} >Author</TableCell>
            <TableCell sx={{color : "white" , fontWeight : "bold" }} >Description</TableCell>
            <TableCell sx={{color : "white" , fontWeight : "bold" }} >Genre</TableCell>
            <TableCell sx={{color : "white" , fontWeight : "bold" }} >Update</TableCell>
            <TableCell sx={{color : "white" , fontWeight : "bold" }} >Delete</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {books.filter( data => {
            if(filteredBook === "") return true;
            else if( JSON.stringify(data).includes(filteredBook)) return true;
            else return false;
          }).map((book) => (
            <TableRow key={book.id}>
              <TableCell>{book.id}</TableCell>
              <TableCell>{book.title}</TableCell>
              <TableCell>{book.author}</TableCell>
              <TableCell sx={{maxWidth : 300 , minWidth :300}} >{book.description}</TableCell>
              <TableCell>{book.genre}</TableCell>
              <TableCell>
                <IconButton onClick={() => { 
                  setUpdate(true);
                  setBookData({
                    id : book.id,
                    title : book.title,
                    author : book.author,
                    description : book.description,
                    genre : book.genre
                  })
                  setFormModal(true);
              }
                } > <EditIcon color={"primary.main"} /> </IconButton>
              </TableCell>
              <TableCell>
              <IconButton onClick={()=>handleDelete(book.id)}> <DeleteIcon color={"primary.main"} /> </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}