import { createTheme, ThemeProvider } from '@mui/material';
import './App.css';
import ListOfBooks from './components/ListOfBooks';

function App() {


  const theme = createTheme({
    palette:{
      primary:{
        main : "#028A0F"
      }
    }
  })


  return (
    <ThemeProvider theme={theme} >
        <ListOfBooks />
    </ThemeProvider>
  );
}

export default App;
